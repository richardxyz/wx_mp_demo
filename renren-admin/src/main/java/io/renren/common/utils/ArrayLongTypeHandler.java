package io.renren.common.utils;

public class ArrayLongTypeHandler {

    public static String[] longToString(Long[] array){
        if(array == null || array.length == 0) return null;
        String[] strArr = new String[array.length];
        for(int i=0;i<strArr.length;i++){
            strArr[i] = String.valueOf(array[i]);
        }
        return strArr;
    }

    //将字符串转成长整型数组

    public static Long[] StringtoLong(String str, String regex) {
        String strs[] = str.split(regex);
        Long array[] = new Long[strs.length];
        for (int i = 0; i < strs.length; i++) {
            array[i] = Long.parseLong(strs[i]);
        }
        return array;
    }
}
