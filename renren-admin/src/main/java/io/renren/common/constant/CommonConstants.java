package io.renren.common.constant;

public interface CommonConstants {
    // 是否第三方平台应用（0：是；1：否）
    /**
     * 0：是，1：否
     */
    Integer YES = 0;

    Integer NO = 1;
}
