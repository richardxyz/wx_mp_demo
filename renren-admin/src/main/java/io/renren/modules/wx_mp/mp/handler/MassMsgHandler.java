/**
 * Copyright (C) 2018-2019
 * All rights reserved, Designed By www.joolun.com
 * 注意：
 * 本软件为www.joolun.com开发研制，未经购买不得使用
 * 购买后可获得全部源代码（禁止转卖、分享、上传到码云、github等开源平台）
 * 一经发现盗用、分享等行为，将追究法律责任，后果自负
 */
package io.renren.modules.wx_mp.mp.handler;

import io.renren.modules.wx_mp.entity.WxAppEntity;
import io.renren.modules.wx_mp.service.WxAppService;
import lombok.AllArgsConstructor;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 群发结果回调
 *
 * @author JL
 */
@Component
@AllArgsConstructor
public class MassMsgHandler extends AbstractHandler {

    private final WxAppService wxAppService;
//	private final WxMassMsgService wxMassMsgService;

    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage, Map<String, Object> context, WxMpService wxMpService,
                                    WxSessionManager sessionManager) {
        // TODO 组装回复消息
        WxAppEntity wxApp = wxAppService.findByWeixinSign(wxMessage.getToUser());
//		TenantContextHolder.setTenantId(wxApp.getTenantId());//加入租户ID
        String msgId = String.valueOf(wxMessage.getMsgId());
//		WxMassMsg wxMassMsg = wxMassMsgService.getOne(Wrappers.query(new WxMassMsg())
////				.eq("tenant_id",wxApp.getTenantId())
//				.eq("app_id",wxApp.getId())
//				.eq("msg_id",msgId));
//		String errCode = wxMessage.getStatus();
//		wxMassMsg.setMsgStatus(WxConsts.MassMsgStatus.SEND_SUCCESS.equals(errCode) ? ConfigConstant.WX_MASS_STATUS_SEND_SUCCESS : ConfigConstant.WX_MASS_STATUS_SEND_FAIL);
//		wxMassMsg.setErrorCode(errCode);
//		wxMassMsg.setErrorMsg(WxConsts.MassMsgStatus.STATUS_DESC.get(errCode));
//		wxMassMsg.setTotalCount(wxMessage.getTotalCount());
//		wxMassMsg.setSentCount(wxMessage.getSentCount());
//		wxMassMsg.setErrorCount(wxMessage.getErrorCount());
//		wxMassMsg.setFilterCount(wxMessage.getFilterCount());
//		wxMassMsgService.updateById(wxMassMsg);
        return null;
    }

}
