package io.renren.modules.wx_mp.controller;

import cn.binarywang.wx.miniapp.api.WxMaUserService;
import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import cn.hutool.core.bean.BeanUtil;
import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.ConfigConstant;
import io.renren.common.constant.Constant;
import io.renren.common.constant.WxReturnCode;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.wx_mp.dto.WxAppDTO;
import io.renren.modules.wx_mp.dto.WxOpenDataDTO;
import io.renren.modules.wx_mp.entity.WxAppEntity;
import io.renren.modules.wx_mp.entity.WxUserEntity;
import io.renren.modules.wx_mp.excel.WxAppExcel;
import io.renren.modules.wx_mp.mp.config.ma.WxMaConfiguration;
import io.renren.modules.wx_mp.mp.config.mp.WxMpConfiguration;
import io.renren.modules.wx_mp.mp.config.pay.WxPayConfiguration;
import io.renren.modules.wx_mp.service.WxAppService;
import io.renren.modules.wx_mp.service.WxUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpQrcodeService;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.WxMpUserService;
import me.chanjar.weixin.mp.bean.result.WxMpQrCodeTicket;
import me.chanjar.weixin.mp.bean.result.WxMpUserList;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 微信公众号
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-25
 */
@RestController
@RequestMapping("wx_mp/wxapp")
@Api(tags = "微信公众号")
public class WxAppController {
    @Autowired
    private WxAppService wxAppService;

    @Autowired
    private WxUserService wxUserService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType = "String")
    })
    @RequiresPermissions("wx_mp:wxapp:page")
    public Result<PageData<WxAppDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params) {
        PageData<WxAppDTO> page = wxAppService.page(params);

        return new Result<PageData<WxAppDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("wx_mp:wxapp:info")
    public Result<WxAppDTO> get(@PathVariable("id") Long id) {
        WxAppDTO data = wxAppService.get(id);

        return new Result<WxAppDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("wx_mp:wxapp:save")
    public Result save(@RequestBody WxAppDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        wxAppService.save(dto);
        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("wx_mp:wxapp:update")
    public Result update(@RequestBody WxAppDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        wxAppService.update(dto);
        WxMpConfiguration.removeWxMpService(dto.getAppId());
        WxPayConfiguration.removeWxPayService(dto.getAppId());
        WxMaConfiguration.removeWxMaService(dto.getAppId());
        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("wx_mp:wxapp:delete")
    public Result delete(@RequestBody Long[] ids) {
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        wxAppService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("wx_mp:wxapp:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<WxAppDTO> list = wxAppService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, WxAppExcel.class);
    }

    /**
     * 获取access-token
     *
     * @param appId 应用id
     * @return
     */
    @GetMapping("/getAccessToken")
    public Result getAccessToken(String appId) {
        System.out.println("appId：" + appId);
        try {
            WxMpService wxMpService = WxMpConfiguration.getMpService(appId);
            return new Result().ok(wxMpService.getAccessToken());
        } catch (WxErrorException e) {
            e.printStackTrace();
            System.out.println("获取access-token失败appID:" + appId + ":" + e.getMessage());
            return new Result().ok(WxReturnCode.wxErrorExceptionHandler(e));
        }
    }


    /**
     * 保存微信用户
     *
     * @param wxOpenDataDTO
     * @return
     */
    @PostMapping("/saveInside")
    public Result saveInside(@RequestBody WxOpenDataDTO wxOpenDataDTO) {
        WxMaUserService wxMaUserService = WxMaConfiguration.getMaService(wxOpenDataDTO.getAppId()).getUserService();
        WxMaUserInfo wxMaUserInfo = wxMaUserService.getUserInfo(wxOpenDataDTO.getSessionKey(), wxOpenDataDTO.getEncryptedData(), wxOpenDataDTO.getIv());
        WxUserEntity wxUser = new WxUserEntity();
        BeanUtil.copyProperties(wxMaUserInfo, wxUser);
        wxUser.setId(wxOpenDataDTO.getUserId());
        wxUser.setAppId(wxOpenDataDTO.getAppId());
        wxUser.setSex(wxMaUserInfo.getGender());
        wxUser.setHeadimgUrl(wxMaUserInfo.getAvatarUrl());
        wxUserService.updateById(wxUser);
        return new Result().ok(wxUser);
    }
}