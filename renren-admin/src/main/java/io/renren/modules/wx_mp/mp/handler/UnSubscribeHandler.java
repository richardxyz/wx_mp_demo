/**
 * Copyright (C) 2018-2019
 * All rights reserved, Designed By www.joolun.com
 * 注意：
 * 本软件为www.joolun.com开发研制，未经购买不得使用
 * 购买后可获得全部源代码（禁止转卖、分享、上传到码云、github等开源平台）
 * 一经发现盗用、分享等行为，将追究法律责任，后果自负
 */
package io.renren.modules.wx_mp.mp.handler;

import io.renren.common.constant.ConfigConstant;
import io.renren.modules.wx_mp.entity.WxAppEntity;
import io.renren.modules.wx_mp.entity.WxUserEntity;
import io.renren.modules.wx_mp.service.WxAppService;
import io.renren.modules.wx_mp.service.WxUserService;
import lombok.AllArgsConstructor;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

/**
 * 用户取消关注
 *
 * @author JL
 */
@Component
@AllArgsConstructor
public class UnSubscribeHandler extends AbstractHandler {

    private final WxAppService wxAppService;
    private final WxUserService wxUserService;
//	private final WxMsgService wxMsgService;
//	private final SimpMessagingTemplate simpMessagingTemplate;

    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage,
                                    Map<String, Object> context, WxMpService wxMpService,
                                    WxSessionManager sessionManager) {
        String openId = wxMessage.getFromUser();
        System.out.println("取消关注用户 OPENID: " + openId);
        // TODO 更新本地数据库为取消关注状态
        WxAppEntity wxApp = wxAppService.findByWeixinSign(wxMessage.getToUser());
//		TenantContextHolder.setTenantId(wxApp.getTenantId());//加入租户ID
        WxUserEntity wxUser = wxUserService.getByOpenId(wxApp.getAppId(), openId);
        if (wxUser != null) {
            wxUser.setSubscribe(ConfigConstant.SUBSCRIBE_TYPE_NO);
            wxUser.setCancelSubscribeTime(new Date());
            wxUserService.updateById(wxUser);
            //消息记录
//			MsgHandler.getWxMpXmlOutMessage(wxMessage,null,wxApp,wxUser,wxMsgService,simpMessagingTemplate);
        }
        return null;
    }

}
