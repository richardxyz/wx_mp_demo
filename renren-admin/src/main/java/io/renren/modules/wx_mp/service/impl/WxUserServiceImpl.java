package io.renren.modules.wx_mp.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.enums.SqlMethod;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.toolkit.Assert;
import com.baomidou.mybatisplus.core.toolkit.ReflectionKit;
import com.baomidou.mybatisplus.core.toolkit.TableInfoHelper;
import com.google.common.collect.Lists;
import io.renren.common.page.PageData;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.ArrayLongTypeHandler;
import io.renren.modules.wx_mp.dao.WxUserDao;
import io.renren.modules.wx_mp.dto.WxUserDTO;
import io.renren.modules.wx_mp.entity.WxAppEntity;
import io.renren.modules.wx_mp.entity.WxUserEntity;
import io.renren.modules.wx_mp.mp.config.mp.WxMpConfiguration;
import io.renren.modules.wx_mp.mp.handler.SubscribeHandler;
import io.renren.modules.wx_mp.service.WxAppService;
import io.renren.modules.wx_mp.service.WxUserService;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpUserService;
import me.chanjar.weixin.mp.api.WxMpUserTagService;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import me.chanjar.weixin.mp.bean.result.WxMpUserList;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.binding.MapperMethod.ParamMap;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.*;


/**
 * 微信用户
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-25
 */
@Service
public class WxUserServiceImpl extends CrudServiceImpl<WxUserDao, WxUserEntity, WxUserDTO> implements WxUserService {

    @Override
    public QueryWrapper<WxUserEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<WxUserEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

    @Autowired
    private WxUserDao wxUserDao;

    @Autowired
    private WxAppService wxAppService;
    @Autowired
    private WxUserService wxUserService;

    @Override
    public WxUserEntity getByOpenId(String appId, String openId) {
        return wxUserDao.getByOpenId(appId, openId);
    }

    @Override
    public void synchroWxUser(String appId) throws WxErrorException {
        //先将已关注的用户取关
//        WxUserEntity wxUser = new WxUserEntity();
//        WxUserEntity wxUser = wxUserDao.selectWxUserByAppIdAndSubscribe
////                (appId, ConfigConstant.SUBSCRIBE_TYPE_YES);
////        wxUser.setSubscribe(ConfigConstant.SUBSCRIBE_TYPE_NO);
////        wxUserDao.updateById(wxUser);
        WxMpUserService wxMpUserService = WxMpConfiguration.getMpService(appId).getUserService();
        WxAppEntity wxApp = wxAppService.findByAppId(appId);
        this.recursionGet(wxApp, wxMpUserService, null);
    }


    /**
     * 递归获取
     *
     * @param nextOpenid
     */
    void recursionGet(WxAppEntity wxApp, WxMpUserService wxMpUserService, String nextOpenid) throws WxErrorException {
        WxMpUserList userList = wxMpUserService.userList(nextOpenid);
        List<WxUserEntity> listWxUser = new ArrayList<>();
        List<WxMpUser> listWxMpUser = getWxMpUserList(wxMpUserService, userList.getOpenids());
        listWxMpUser.forEach(wxMpUser -> {
            WxUserEntity wxUser = this.getByOpenId(wxApp.getAppId(), wxMpUser.getOpenId());
            if (wxUser == null) {//用户未存在
                wxUser = new WxUserEntity();
                wxUser.setSubscribeNum(1);
            }
            SubscribeHandler.setWxUserValue(wxApp, wxUser, wxMpUser);
            listWxUser.add(wxUser);
        });
        for (WxUserEntity userEntity : listWxUser) {
            try {
                wxUserService.insert(userEntity);
            } catch (Exception e) {
                wxUserService.updateById(userEntity);
            }
        }
        if (userList.getCount() >= 10000) {
            this.recursionGet(wxApp, wxMpUserService, userList.getNextOpenid());
        }
    }


    /**
     * 分批次获取微信粉丝信息 每批100条
     *
     * @param wxMpUserService
     * @param openidsList
     * @return
     * @throws WxErrorException
     * @author
     */
    private List<WxMpUser> getWxMpUserList(WxMpUserService wxMpUserService, List<String> openidsList) throws WxErrorException {
        // 粉丝openid数量
        int count = openidsList.size();
        if (count <= 0) {
            return new ArrayList<>();
        }
        List<WxMpUser> list = Lists.newArrayList();
        List<WxMpUser> followersInfoList;
        int a = count % 100 > 0 ? count / 100 + 1 : count / 100;
        for (int i = 0; i < a; i++) {
            if (i + 1 < a) {
                System.out.println("i:{},from:{},to:{}" + i + " " + i * 100 + " " + (i + 1) * 100);
                followersInfoList = wxMpUserService.userInfoList(openidsList.subList(i * 100, ((i + 1) * 100)));
                if (null != followersInfoList && !followersInfoList.isEmpty()) {
                    list.addAll(followersInfoList);
                }
            } else {
                System.out.println("i:{},from:{},to:{}" + i + " " + i * 100 + " " + (count - i * 100));
                followersInfoList = wxMpUserService.userInfoList(openidsList.subList(i * 100, count));
                if (null != followersInfoList && !followersInfoList.isEmpty()) {
                    list.addAll(followersInfoList);
                }
            }
        }
        System.out.println("本批次获取微信粉丝数：" + list.size());
        return list;
    }


//    @Override
//    public boolean updateRemark(WxUserEntity entity) throws WxErrorException {
//        Long id = entity.getId();
//        String remark = entity.getRemark();
//        String appId = entity.getAppId();
//        String openId = entity.getOpenId();
//        entity = new WxUserEntity();
//        entity.setId(id);
//        entity.setRemark(remark);
//        super.updateById(entity);
//        WxMpUserService wxMpUserService = WxMpConfiguration.getMpService(appId).getUserService();
//        wxMpUserService.userUpdateRemark(openId, remark);
//        return true;
//    }


    @Override
    public boolean updateRemark(Long id, String remark) throws WxErrorException {
        WxUserEntity userEntity = wxUserDao.selectById(id);
        userEntity.setRemark(remark);
        wxUserDao.updateById(userEntity);
        WxMpUserService wxMpUserService = WxMpConfiguration.getMpService(userEntity.getAppId()).getUserService();
        wxMpUserService.userUpdateRemark(userEntity.getOpenId(), remark);
        return true;
    }

    @Override
    public void tagging(String taggingType, String appId, Long tagId, String[] openIds) throws WxErrorException {
        WxMpUserTagService wxMpUserTagService = WxMpConfiguration.getMpService(appId).getUserTagService();
        WxUserEntity wxUser;
        if ("tagging".equals(taggingType)) {
            for (String openId : openIds) {
                wxUser = this.getByOpenId(appId, openId);
                Long[] tagidList = ArrayLongTypeHandler.StringtoLong(wxUser.getTagidList(), "");
                List<Long> list = Arrays.asList(tagidList);
                list = new ArrayList<>(list);
                if (!list.contains(tagId)) {
                    list.add(tagId);
                    tagidList = list.toArray(new Long[list.size()]);
                    String[] tagidLists = ArrayLongTypeHandler.longToString(tagidList);
                    wxUser.setTagidList(tagidLists + "");
                    this.updateById(wxUser);
                }
            }
            wxMpUserTagService.batchTagging(tagId, openIds);
        }
        if ("unTagging".equals(taggingType)) {
            for (String openId : openIds) {
                wxUser = this.getByOpenId(appId, openId);
                Long[] tagidList = ArrayLongTypeHandler.StringtoLong(wxUser.getTagidList(), "");
                List<Long> list = Arrays.asList(tagidList);
                list = new ArrayList<>(list);
                if (list.contains(tagId)) {
                    list.remove(tagId);
                    tagidList = list.toArray(new Long[list.size()]);
                    String[] tagidLists = ArrayLongTypeHandler.longToString(tagidList);
                    wxUser.setTagidList(tagidLists + "");
                    this.updateById(wxUser);
                }
            }
            wxMpUserTagService.batchUntagging(tagId, openIds);
        }
    }

    @Override
    public List<WxUserEntity> findAlllist(String appId) {
        return wxUserDao.findAlllist(appId);
    }

    @Override
    public PageData<WxUserDTO> getPageByAppId(Map<String, Object> params, String appId) {
        IPage<WxUserEntity> page = baseDao.selectPage(
                getPage(params, null, false),
                getWrapper(params).eq("app_id", appId)
        );

        return getPageData(page, currentDtoClass());
    }


}