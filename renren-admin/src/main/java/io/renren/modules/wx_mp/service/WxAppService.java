package io.renren.modules.wx_mp.service;

import io.renren.common.service.CrudService;
import io.renren.modules.wx_mp.dto.WxAppDTO;
import io.renren.modules.wx_mp.entity.WxAppEntity;

/**
 * 微信公众号
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-25
 */
public interface WxAppService extends CrudService<WxAppEntity, WxAppDTO> {

    /**
     * 微信原始标识查找
     * @param weixinSign
     * @return
     */
    WxAppEntity findByWeixinSign(String weixinSign);

    /**
     * 通过appId获取WxApp，无租户条件
     * @param appId
     * @return
     */
    WxAppEntity findByAppId(String appId);
}