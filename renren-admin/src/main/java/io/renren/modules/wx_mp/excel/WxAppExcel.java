package io.renren.modules.wx_mp.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 微信公众号
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-25
 */
@Data
public class WxAppExcel {
    @Excel(name = "主键")
    private Long id;
    @Excel(name = "微信原始标识")
    private String weixinSign;
    @Excel(name = "token")
    private String token;
    @Excel(name = "EncodingAESKey")
    private String aesKey;
    @Excel(name = "微信号昵称")
    private String name;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}