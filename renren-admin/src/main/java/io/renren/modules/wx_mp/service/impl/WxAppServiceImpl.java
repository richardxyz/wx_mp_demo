package io.renren.modules.wx_mp.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.wx_mp.dao.WxAppDao;
import io.renren.modules.wx_mp.dto.WxAppDTO;
import io.renren.modules.wx_mp.entity.WxAppEntity;
import io.renren.modules.wx_mp.service.WxAppService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 微信公众号
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-25
 */
@Service
public class WxAppServiceImpl extends CrudServiceImpl<WxAppDao, WxAppEntity, WxAppDTO> implements WxAppService {

    @Override
    public QueryWrapper<WxAppEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<WxAppEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

    @Autowired
    private WxAppDao wxAppDao;

    @Override
    public WxAppEntity findByWeixinSign(String weixinSign) {
        return wxAppDao.findByWeixinSign(weixinSign);
    }

    @Override
    public WxAppEntity findByAppId(String appId) {
        return wxAppDao.findByAppId(appId);
    }
}