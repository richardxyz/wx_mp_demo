/**
 * Copyright (C) 2018-2019
 * All rights reserved, Designed By www.joolun.com
 * 注意：
 * 本软件为www.joolun.com开发研制，未经购买不得使用
 * 购买后可获得全部源代码（禁止转卖、分享、上传到码云、github等开源平台）
 * 一经发现盗用、分享等行为，将追究法律责任，后果自负
 */
package io.renren.modules.wx_mp.mp.handler;

import cn.hutool.json.JSONUtil;
import io.renren.common.constant.ConfigConstant;
import io.renren.common.utils.ArrayLongTypeHandler;
import io.renren.common.utils.LocalDateTimeUtil;
import io.renren.modules.wx_mp.entity.WxAppEntity;
import io.renren.modules.wx_mp.entity.WxUserEntity;
import io.renren.modules.wx_mp.service.WxAppService;
import io.renren.modules.wx_mp.service.WxUserService;
import lombok.AllArgsConstructor;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author JL
 * 用户关注
 */
@Component
@AllArgsConstructor
public class SubscribeHandler extends AbstractHandler {

    //	private final WxAutoReplyService wxAutoReplyService;
    private final WxAppService wxAppService;
    private final WxUserService wxUserService;
//    	private final WxMsgService wxMsgService;
//    private final SimpMessagingTemplate simpMessagingTemplate;

    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage,
                                    Map<String, Object> context, WxMpService weixinService,
                                    WxSessionManager sessionManager) {
        System.out.println("新关注用户 OPENID: " + wxMessage.getFromUser());
        // 获取微信用户基本信息
        try {
            WxMpUser userWxInfo = weixinService.getUserService()
                    .userInfo(wxMessage.getFromUser(), null);
            if (userWxInfo != null) {
                // TODO 添加关注用户到本地数据库
                WxAppEntity wxApp = wxAppService.findByWeixinSign(wxMessage.getToUser());
//				TenantContextHolder.setTenantId(wxApp.getTenantId());//加入租户ID
                if (wxApp != null) {
                    WxUserEntity wxUser = wxUserService.getByOpenId(wxApp.getAppId(), userWxInfo.getOpenId());
                    if (wxUser == null) {//第一次关注
                        wxUser = new WxUserEntity();
                        wxUser.setSubscribeNum(1);
                        this.setWxUserValue(wxApp, wxUser, userWxInfo);
//						wxUser.setTenantId(wxApp.getTenantId());
                        wxUserService.insert(wxUser);
                    } else {//曾经关注过
                        wxUser.setSubscribeNum(wxUser.getSubscribeNum() + 1);
                        this.setWxUserValue(wxApp, wxUser, userWxInfo);
//						wxUser.setTenantId(wxApp.getTenantId());
                        wxUserService.updateById(wxUser);
                    }
                    //发送关注消息
//					List<WxAutoReply> listWxAutoReply = wxAutoReplyService.list(Wrappers.<WxAutoReply>query()
//							.lambda().eq(WxAutoReply::getAppId, wxApp.getId()).eq(WxAutoReply::getType, ConfigConstant.WX_AUTO_REPLY_TYPE_1));
//					WxMpXmlOutMessage wxMpXmlOutMessage = MsgHandler.getWxMpXmlOutMessage(wxMessage,listWxAutoReply,wxApp,wxUser,wxMsgService,simpMessagingTemplate);
//					return wxMpXmlOutMessage;
                    return null;
                }
            }
        } catch (Exception e) {
            System.out.println("用户关注出错：" + e.getMessage());
        }
        return null;
    }

    public static void setWxUserValue(WxAppEntity wxApp, WxUserEntity wxUser, WxMpUser userWxInfo) {
        wxUser.setAppType(ConfigConstant.WX_APP_TYPE_2);
        wxUser.setAppId(wxApp.getAppId());
        wxUser.setSubscribe(ConfigConstant.SUBSCRIBE_TYPE_YES);
        wxUser.setSubscribeScene(userWxInfo.getSubscribeScene());
        wxUser.setSubscribeTime(LocalDateTimeUtil.timestamToDatetime(userWxInfo.getSubscribeTime() * 1000));
        wxUser.setOpenId(userWxInfo.getOpenId());
        wxUser.setNickName(userWxInfo.getNickname());
        wxUser.setSex(String.valueOf(userWxInfo.getSex()));
        wxUser.setCity(userWxInfo.getCity());
        wxUser.setCountry(userWxInfo.getCountry());
        wxUser.setProvince(userWxInfo.getProvince());
        wxUser.setLanguage(userWxInfo.getLanguage());
        wxUser.setRemark(userWxInfo.getRemark());
        wxUser.setHeadimgUrl(userWxInfo.getHeadImgUrl());
        wxUser.setUnionId(userWxInfo.getUnionId());
        wxUser.setGroupId(JSONUtil.toJsonStr(userWxInfo.getGroupId()));
        String[] tagidLists = ArrayLongTypeHandler.longToString(userWxInfo.getTagIds());
        wxUser.setTagidList(tagidLists + "");
        wxUser.setQrSceneStr(userWxInfo.getQrSceneStr());
    }
}
