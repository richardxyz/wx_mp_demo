package io.renren.modules.wx_mp.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "微信用户加密数据")
public class ThirdSession implements Serializable {
    /**
     * 微信用户ID
     */
    private Long wxUserId;
    /**
     * 配置项ID
     */
    private String appId;
    /**
     * 微信sessionKey
     */
    private String sessionKey;
    /**
     * 用户标识
     */
    private String openId;
}
