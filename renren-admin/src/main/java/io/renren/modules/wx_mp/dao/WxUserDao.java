package io.renren.modules.wx_mp.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.wx_mp.entity.WxUserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 微信用户
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-25
 */
@Mapper
public interface WxUserDao extends BaseDao<WxUserEntity> {

    @Select("SELECT * FROM wx_user WHERE app_id = #{app_id} AND open_id = #{open_id}")
    WxUserEntity getByOpenId(@Param("app_id") String appId, @Param("open_id") String openId);

    @Select("SELECT * FROM wx_user WHERE app_id = #{app_id} AND subscribe = #{subscribe}")
    WxUserEntity selectWxUserByAppIdAndSubscribe(@Param("app_id") String appId, @Param("subscribe") String subscribeTypeYes);

    @Select("SELECT * FROM wx_user WHERE app_id = #{app_id}")
    List<WxUserEntity> findAlllist( @Param("app_id")String appId);
}