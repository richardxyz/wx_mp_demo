package io.renren.modules.wx_mp.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.wx_mp.entity.WxAppEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 微信公众号
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-25
 */
@Mapper
public interface WxAppDao extends BaseDao<WxAppEntity> {

    @Select("SELECT * FROM wx_app WHERE `weixin_sign` = #{weixin_sign}")
    WxAppEntity findByWeixinSign(@Param("weixin_sign") String weixinSign);

    @Select("SELECT * FROM wx_app WHERE `app_id` = #{app_id}")
    WxAppEntity findByAppId(@Param("app_id")String appId);
}