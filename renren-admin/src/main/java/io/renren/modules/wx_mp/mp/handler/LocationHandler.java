/**
 * Copyright (C) 2018-2019
 * All rights reserved, Designed By www.joolun.com
 * 注意：
 * 本软件为www.joolun.com开发研制，未经购买不得使用
 * 购买后可获得全部源代码（禁止转卖、分享、上传到码云、github等开源平台）
 * 一经发现盗用、分享等行为，将追究法律责任，后果自负
 */
package io.renren.modules.wx_mp.mp.handler;

import io.renren.modules.wx_mp.entity.WxAppEntity;
import io.renren.modules.wx_mp.entity.WxUserEntity;
import io.renren.modules.wx_mp.service.WxAppService;
import io.renren.modules.wx_mp.service.WxUserService;
import lombok.AllArgsConstructor;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.springframework.stereotype.Component;

import java.util.Map;


/**
 * @author JL
 */
@Component
@AllArgsConstructor
public class LocationHandler extends AbstractHandler {

    private final WxUserService wxUserService;
    private final WxAppService wxAppService;

    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage,
                                    Map<String, Object> context, WxMpService wxMpService,
                                    WxSessionManager sessionManager) {
        if (wxMessage.getEvent().equals(WxConsts.EventType.LOCATION)) {
            try {
                WxAppEntity wxApp = wxAppService.findByWeixinSign(wxMessage.getToUser());
//				TenantContextHolder.setTenantId(wxApp.getTenantId());//加入租户ID
                WxUserEntity wxUser = wxUserService.getByOpenId(wxApp.getAppId(), wxMessage.getFromUser());
//                wxUser.setLatitude(wxMessage.getLatitude());
//                wxUser.setLongitude(wxMessage.getLongitude());
//                wxUser.setPrecision(wxMessage.getPrecision());
                wxUserService.updateById(wxUser);
                return null;
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("位置消息接收处理失败"+ e);
                return null;
            }
        }
        return null;
    }

}
