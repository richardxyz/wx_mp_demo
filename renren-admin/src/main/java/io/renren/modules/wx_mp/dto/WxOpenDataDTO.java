package io.renren.modules.wx_mp.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 微信开发数据
 */
@Data
@ApiModel(value = "微信开发数据")
public class WxOpenDataDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "应用id")
    private String appId;

    @ApiModelProperty(value = "用户id")
    private Long userId;

    @ApiModelProperty(value = "包括敏感数据在内的完整用户信息的加密数据")
    private String encryptedData;

    @ApiModelProperty(value = "错误信息")
    private String errMsg;

    @ApiModelProperty(value = "加密算法的初始向量")
    private String iv;

    @ApiModelProperty(value = "不包括敏感信息的原始数据字符串，用于计算签名")
    private String rawData;

    @ApiModelProperty(value = "使用 sha1( rawData + sessionkey ) 得到字符串，用于校验用户信息")
    private String signature;

    @ApiModelProperty(value = "签名校验以及数据加解密涉及用户的会话密钥 ")
    private String sessionKey;
}