package io.renren.modules.wx_mp.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 微信公众号
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-25
 */
@Data
@ApiModel(value = "微信公众号")
public class WxAppDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "小程序/公众号ID")
    private String appId;

    @ApiModelProperty(value = "应用类型(1:小程序，2:公众号)")
    private String appType;

    @ApiModelProperty(value = "微信原始标识")
    private String weixinSign;

    @ApiModelProperty(value = "应用密钥")
    private String secret;

    @ApiModelProperty(value = "token")
    private String token;

    @ApiModelProperty(value = "EncodingAESKey")
    private String aesKey;

    @ApiModelProperty(value = "微信号昵称")
    private String name;

    @ApiModelProperty(value = "是否第三方平台应用（0：是；1：否）")
    private String isComponent;

    @ApiModelProperty(value = "微信支付商户号")
    private String mchId;

    @ApiModelProperty(value = "微信支付商户密钥")
    private String mchKey;

    @ApiModelProperty(value = "p12证书的位置，可以指定绝对路径，也可以指定类路径（以classpath:开头）")
    private String keyPath;
    @ApiModelProperty(value = "更新者")
    private Long updater;

    @ApiModelProperty(value = "更新时间")
    private Date updateDate;

    @ApiModelProperty(value = "创建者")
    private Long creator;

    @ApiModelProperty(value = "创建时间")
    private Date createDate;


}