/**
 * Copyright (C) 2018-2019
 * All rights reserved, Designed By www.joolun.com
 * 注意：
 * 本软件为www.joolun.com开发研制，未经购买不得使用
 * 购买后可获得全部源代码（禁止转卖、分享、上传到码云、github等开源平台）
 * 一经发现盗用、分享等行为，将追究法律责任，后果自负
 */
package io.renren.modules.wx_mp.mp.config.open;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

/**
 * 第三方平台账号配置
 *
 * @author
 */
@Data
@RefreshScope
@Configuration
@ConfigurationProperties(prefix = "wx.component")
public class WxComponentConfigProperties {
    /**
     * 设置微信公众号的appid
     */
    private String appId = "appId";
    /**
     * 设置微信公众号的app secret
     */
    private String appSecret = "appSecret";
    /**
     * 设置微信公众号的token
     */
    private String token = "token";
    /**
     * 设置微信公众号的EncodingAESKey
     */
    private String aesKey = "aesKey";

}
