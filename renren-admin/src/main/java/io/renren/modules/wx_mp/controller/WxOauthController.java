package io.renren.modules.wx_mp.controller;

import cn.binarywang.wx.miniapp.api.WxMaUserService;
import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import cn.hutool.core.bean.BeanUtil;
import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.constant.WeiXinConstant;
import io.renren.common.constant.WxReturnCode;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.wx_mp.dto.WxAppDTO;
import io.renren.modules.wx_mp.dto.WxOpenDataDTO;
import io.renren.modules.wx_mp.entity.WxUserEntity;
import io.renren.modules.wx_mp.excel.WxAppExcel;
import io.renren.modules.wx_mp.mp.config.ma.WxMaConfiguration;
import io.renren.modules.wx_mp.mp.config.mp.WxMpConfiguration;
import io.renren.modules.wx_mp.mp.config.pay.WxPayConfiguration;
import io.renren.modules.wx_mp.service.WxAppService;
import io.renren.modules.wx_mp.service.WxUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;


/**
 * 微信公众号
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-25
 */
@RestController
@RequestMapping("wx_mp/weixin")
@Api(tags = "微信公众号")
public class WxOauthController {
    /**
     * 第一步：用户同意授权，获取code
     *
     * @param response
     * @throws IOException
     */
    @GetMapping("/oauth")
    public void oauth(HttpServletResponse response) throws IOException {
        String path = WeiXinConstant.REAL_URL + "wx_mp/weixin/invoke";
        try {
            path = URLEncoder.encode(path, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url = "https://open.weixin.qq.com/connect/oauth2/authorize" +
                "?appid=" + WeiXinConstant.appId +
                "&redirect_uri=" + path +
                "&response_type=code" +
                "&scope=snsapi_userinfo" +
                "&state=STATE" +
                "#wechat_redirect";
        response.sendRedirect(url);
    }


    /**
     *
     */
    @GetMapping("invoke")//域名+wx_mp/weixin/invoke
    public void oauthInvoke(HttpServletRequest request) {
        // 获取code
        String code = request.getParameter("code");
        String state = request.getParameter("state");
        System.out.println("code：" + code);
        System.out.println("state：" + state);

    }

}