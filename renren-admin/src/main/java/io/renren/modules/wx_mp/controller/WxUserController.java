package io.renren.modules.wx_mp.controller;

import com.baomidou.mybatisplus.extension.api.R;
import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.constant.MyReturnCode;
import io.renren.common.constant.WxReturnCode;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.wx_mp.dto.WxUserDTO;
import io.renren.modules.wx_mp.entity.WxUserEntity;
import io.renren.modules.wx_mp.excel.WxUserExcel;
import io.renren.modules.wx_mp.service.WxUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.common.error.WxErrorException;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * 微信用户
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-25
 */
@RestController
@RequestMapping("wx_mp/wxuser")
@Api(tags = "微信用户")
public class WxUserController {
    @Autowired
    private WxUserService wxUserService;

    @GetMapping("getPageByAppId/{appId}")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType = "String")
    })
//    @RequiresPermissions("wx_mp:wxuser:page")
    public Result<PageData<WxUserDTO>> getPageByAppId(@ApiIgnore @RequestParam Map<String, Object> params, @PathVariable("appId") String appId) {
        System.err.println("appid+" + appId);
        PageData<WxUserDTO> page = wxUserService.getPageByAppId(params, appId);

        return new Result<PageData<WxUserDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("wx_mp:wxuser:info")
    public Result<WxUserDTO> get(@PathVariable("id") Long id) {
        WxUserDTO data = wxUserService.get(id);

        return new Result<WxUserDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("wx_mp:wxuser:save")
    public Result save(@RequestBody WxUserDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        wxUserService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("wx_mp:wxuser:update")
    public Result update(@RequestBody WxUserDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        wxUserService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("wx_mp:wxuser:delete")
    public Result delete(@RequestBody Long[] ids) {
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        wxUserService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("wx_mp:wxuser:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<WxUserDTO> list = wxUserService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, WxUserExcel.class);
    }


    @GetMapping("synchronAllUser")
    public Result<MyReturnCode> synchronAllUser(@RequestParam("appId") String appId) {
        System.err.println("appid====" + appId);
        try {
            wxUserService.synchroWxUser(appId);
            return new Result();
        } catch (WxErrorException e) {
            e.printStackTrace();
            MyReturnCode err60000 = MyReturnCode.ERR_60000;
            return new Result().ok(err60000);
        }
    }


//    /**
//     * 修改微信用户备注
//     *
//     * @param wxUser
//     * @return
//     */
//    @PutMapping("/updateRemark")
//    public Result updateRemark(@RequestBody WxUserEntity wxUser) {
//        try {
//            return new Result().ok(wxUserService.updateRemark(wxUser));
//        } catch (WxErrorException e) {
//            e.printStackTrace();
//            System.out.println("修改微信用户备注失败" + e);
//            return WxReturnCode.wxErrorExceptionHandler(e);
//        }
//    }

    /**
     * 修改微信用户备注
     *
     * @param entity
     * @return
     */
    @PutMapping("/updateRemark")
    public Result updateRemark(@RequestBody WxUserEntity entity) {
        System.out.println(entity.toString());
        try {
            return new Result().ok(wxUserService.updateRemark(entity.getId(),entity.getRemark()));

        } catch (WxErrorException e) {
            e.printStackTrace();
            System.out.println("修改微信用户备注失败" + e);
            return WxReturnCode.wxErrorExceptionHandler(e);
        }

//        return new Result();
    }

}