package io.renren.modules.wx_mp.service;

import io.renren.common.page.PageData;
import io.renren.common.service.CrudService;
import io.renren.modules.wx_mp.dto.WxUserDTO;
import io.renren.modules.wx_mp.entity.WxUserEntity;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.apache.poi.ss.formula.functions.T;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 微信用户
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-25
 */
public interface WxUserService extends CrudService<WxUserEntity, WxUserDTO> {

    /**
     * 根据appId获取用户
     * @param appId
     * @return
     */
//    List<WxUserEntity> getUserByAppId(String appId);

    /**
     * 根据openId获取用户
     *
     * @param appId
     * @param openId
     * @return
     */
    WxUserEntity getByOpenId(String appId, String openId);

    /**
     * 同步微信用户
     *
     * @param appId
     */
    void synchroWxUser(String appId) throws WxErrorException;

//    /**
//     * 修改用户备注
//     *
//     * @param entity
//     * @return
//     */
//    boolean updateRemark(WxUserEntity entity) throws WxErrorException;

    /**
     * 修改用户备注
     * @param id
     * @param remark
     * @return
     * @throws WxErrorException
     */
    boolean updateRemark(Long id,String  remark) throws WxErrorException;

    /**
     * 认识标签
     *
     * @param taggingType
     * @param appId
     * @param tagId
     * @param openIds
     * @throws WxErrorException
     */
    void tagging(String taggingType, String appId, Long tagId, String[] openIds) throws WxErrorException;

    /**
     * 查询所有用户信息
     *
     * @return
     */
    List<WxUserEntity> findAlllist(String appId);

    PageData<WxUserDTO> getPageByAppId(Map<String, Object> params, String appId);

}