package io.renren.modules.wx_mp.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 微信用户
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-25
 */
@Data
@ApiModel(value = "微信用户")
public class WxUserDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息。")
    private String subscribe;

    @ApiModelProperty(value = "用户关注时间，为时间戳。如果用户曾多次关注，则取最后关注时间")
    private Date subscribeTime;

    @ApiModelProperty(value = "返回用户关注的渠道来源，ADD_SCENE_SEARCH 公众号搜索，ADD_SCENE_ACCOUNT_MIGRATION 公众号迁移，ADD_SCENE_PROFILE_CARD 名片分享，ADD_SCENE_QR_CODE 扫描二维码，ADD_SCENE_PROFILE_ LINK 图文页内名称点击，ADD_SCENE_PROFILE_ITEM 图文页右上角菜单，ADD_SCENE_PAID 支付后关注，ADD_SCENE_OTHERS 其他")
    private String subscribeScene;

    @ApiModelProperty(value = "关注次数")
    private Integer subscribeNum;

    @ApiModelProperty(value = "取消关注时间")
    private Date cancelSubscribeTime;

    @ApiModelProperty(value = "小程序/公众号ID")
    private String appId;

    @ApiModelProperty(value = "应用类型(1:小程序，2:公众号)")
    private String appType;

    @ApiModelProperty(value = "用户的标识，对当前公众号唯一")
    private String openId;

    @ApiModelProperty(value = "公众号运营者对粉丝的备注，公众号运营者可在微信公众平台用户管理界面对粉丝添加备注")
    private String remark;

    @ApiModelProperty(value = "用户所在的分组ID（兼容旧的用户分组接口）")
    private Long groupId;

    @ApiModelProperty(value = "用户被打上的标签ID列表")
    private Long[] tagidList;

    @ApiModelProperty(value = "用户的昵称")
    private String nickName;

    @ApiModelProperty(value = "用户的性别，值为1时是男性，值为2时是女性，值为0时是未知")
    private String sex;

    @ApiModelProperty(value = "用户所在城市")
    private String city;

    @ApiModelProperty(value = "用户所在国家")
    private String country;

    @ApiModelProperty(value = "用户所在省份")
    private String province;

    @ApiModelProperty(value = "用户的语言，简体中文为zh_CN")
    private String language;

    @ApiModelProperty(value = "用户头像")
    private String headimgUrl;

    @ApiModelProperty(value = "只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。")
    private String unionId;

    @ApiModelProperty(value = "二维码扫码场景（开发者自定义）")
    private String qrScene;

    @ApiModelProperty(value = "二维码扫码场景描述（开发者自定义）")
    private String qrSceneStr;

    @ApiModelProperty(value = "更新者")
    private Long updater;

    @ApiModelProperty(value = "更新时间")
    private Date updateDate;

    @ApiModelProperty(value = "创建者")
    private Long creator;

    @ApiModelProperty(value = "创建时间")
    private Date createDate;


}