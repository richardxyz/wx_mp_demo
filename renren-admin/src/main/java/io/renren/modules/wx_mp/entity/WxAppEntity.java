package io.renren.modules.wx_mp.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 微信公众号
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-25
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("wx_app")
public class WxAppEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

    /**
     * 小程序/公众号ID
     */
	private String appId;
	/**
	 * 应用类型(1:小程序，2:公众号)
	 */
	private String appType;
    /**
     * 微信原始标识
     */
	private String weixinSign;
	/**
     * 应用密钥
     */
	private String secret;
    /**
     * token
     */
	private String token;
    /**
     * EncodingAESKey
     */
	private String aesKey;
    /**
     * 微信号昵称
     */
	private String name;
	/**
	 * 是否第三方平台应用（0：是；1：否）
	 */
	private String isComponent;
	/**
	 * 微信支付商户号
	 */
	private String mchId;
	/**
	 * 微信支付商户密钥
	 */
	private String mchKey;
	/**
	 * p12证书的位置，可以指定绝对路径，也可以指定类路径（以classpath:开头）
	 */
	private String keyPath;
    /**
     * 更新者
     */
	private Long updater;
    /**
     * 更新时间
     */
	private Date updateDate;
}